FROM node:15

COPY ./src /app/src
COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json
COPY ./webpack.config.js /app/webpack.config.js


WORKDIR /app
RUN npm install

CMD npm run server